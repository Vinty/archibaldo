﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace Player {
    public enum StateMoving {
        WAIT,
        MOVING,
        SHOOT,
        STOP
    }

    [RequireComponent(typeof(NavMeshAgent))]
    public class InputMovement : MonoBehaviour {
        [SerializeField] private GameObject[] enemies;

        private Attack attack;

        private RaycastHit   hitInfo;
        private NavMeshAgent agent;
        public  StateMoving  CurrentStateMoving {get; set;}
        private float        timer = 0;


        private void Start() {
            attack             = FindObjectOfType<Attack>();
            agent              = GetComponent<NavMeshAgent>();
            CurrentStateMoving = StateMoving.WAIT;
        }

        private void Update() {
            ClickLeftMouse();

            switch(CurrentStateMoving) {
                case StateMoving.WAIT : break;
                case StateMoving.STOP :
                    if(timer < Time.time) {
                        CurrentStateMoving = StateMoving.SHOOT;
                    }

                    break;
                case StateMoving.MOVING :
                    attack.currentTypeAttack = TypeAttack.WAIT;
                    MoveToMouse();

                    timer = Time.time + 0.2f;
                    break;
                case StateMoving.SHOOT :
                    Debug.Log("PIU PIU !!!");
                    var target = enemies[0];
                    transform.LookAt(target.transform);

                    attack.currentTypeAttack = TypeAttack.NORMAL;

                    break;
            }
        }

        private void ClickLeftMouse() {
            if(UnityEngine.Camera.main == null) {
                return;
            }

            if(Input.GetMouseButton(0)) {
                StopCoroutine(Move());
                CurrentStateMoving = StateMoving.MOVING;
            }
        }

        private void MoveToMouse() {
            var ray = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray.origin, ray.direction, out hitInfo)) {
                StartCoroutine(Move());
                CurrentStateMoving = StateMoving.WAIT;
            }
        }

        private IEnumerator Move() {
            var distance = Vector3.Distance(gameObject.transform.position, hitInfo.point);

            while(distance > 1.1f) {
                agent.destination = hitInfo.point;
                distance          = Vector3.Distance(gameObject.transform.position, hitInfo.point);
                yield return null;
            }

            CurrentStateMoving = StateMoving.STOP;
        }

        // private void OnDrawGizmos() 
        // {
        //     Debug.DrawLine(_agent.destination, pointPath.position + _moveInput * 5 * Vector3.right, Color.red);
        // }
    }
}
