﻿using System;
using UnityEngine;

namespace Player {
    public enum CharacterState {
        IDLE,
        MOVE,
        ATTACK,
        DEAD,
    }

    // [RequireComponent(typeof(Animator))]
    public class PlayerController : MonoBehaviour {
        // [SerializeField] private Animator animator;
        [SerializeField] private Transform      firePoint;
        private                  InputMovement  inputMovement;
        private                  Attack         attack;
        private                  CharacterState currentCharacterState;

        

        // private void Reset() {
        //     animator = GetComponent<Animator>();
        // }
        private void Start() {
            attack                   = GetComponent<Attack>();
            attack.currentTypeAttack = TypeAttack.WAIT;
            inputMovement            = GetComponent<InputMovement>();
        }

        private void Update() {
            switch(currentCharacterState) {
                case CharacterState.IDLE : break;
                case CharacterState.MOVE : break;
                case CharacterState.ATTACK :
                    Attack();
                    break;
                case CharacterState.DEAD : break;
                default :                  throw new ArgumentOutOfRangeException();
            }
        }

        public Transform GetFirePointTransform() {
            return firePoint;
        }
        
        private void Attack() {
            attack.currentTypeAttack = TypeAttack.NORMAL;
        }
    }
}
