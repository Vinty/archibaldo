﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Camera : MonoBehaviour
{
    [SerializeField] private float posX = 1f;
    [SerializeField] private float posY = 1f;
    [SerializeField] private float posZ = 1f;
    [SerializeField] private float cameraSpeed = 2f;
    [SerializeField] private GameObject _player;

    private void Update()
    {
        Vector3 position = _player.transform.position;
        Vector3 playerPosition = new Vector3(position.x + posX, position.y + posY, position.z + posZ);
        transform.position = Vector3.Lerp(transform.position, playerPosition, Time.deltaTime * cameraSpeed);
    }
}
