﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectsPool : MonoBehaviour {
    private static ObjectsPool instance;

    public static ObjectsPool Instance {
        get {
            if(instance) {
                return instance;
            }

            var obj = new GameObject("ObjectsPool");
            instance = obj.AddComponent<ObjectsPool>();
            return instance;
        }
    }

    private Dictionary<GameObject, List<GameObject>> pool = new Dictionary<GameObject, List<GameObject>>();

    public GameObject GetObject(GameObject prefab) {
        if(!pool.ContainsKey(prefab)) {
            var firstElement = Instantiate(prefab);
            firstElement.SetActive(false);
            pool[prefab] = new List<GameObject> { firstElement };
        }

        List<GameObject> objects = pool[prefab];
        foreach(GameObject obj in objects.Where(obj => obj && !obj.activeSelf)) {
            obj.SetActive(true);
            return obj;
        }

        var newObj = Instantiate(prefab);
        objects.Add(newObj);
        return newObj;
    }
}
