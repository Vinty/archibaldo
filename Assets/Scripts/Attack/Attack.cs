﻿using UnityEngine;

namespace Player {
    public enum TypeAttack {
        WAIT,
        NORMAL,
        DOUBLE
    }

    public class Attack : MonoBehaviour {
        [SerializeField] private GameObject bulletsPrefab;
        [SerializeField] private float      baseAttackSpeed = 1f;
        [SerializeField] private GameObject playerGraphicObject;

        private float            currentAttackSpeed = 1f;
        private float            attackSpeedTimer;
        private PlayerController player;
        public  TypeAttack       currentTypeAttack {get; set;}


        private void Start() {
            player = GetComponent<PlayerController>();
            if(!playerGraphicObject) {
                playerGraphicObject = GameObject.Find("Player/Graphic");
            }

            // 10 : Увеличит на +10% скорость атаки.
            // -20 :УМЕНЬШИТ НА 20% СКОРОСТЬ АТАКИ ОТ базовой (не от текущей).
            ChangeAttackSpeed(0);
        }

        public void Update() {
            switch(currentTypeAttack) {
                case TypeAttack.WAIT : break;
                case TypeAttack.NORMAL :
                    NormalAttack();
                    break;
                case TypeAttack.DOUBLE :
                    DoubleAttack();
                    break;
            }
        }

        private void ChangeAttackSpeed(float attackSpeedModificator) {
            currentAttackSpeed = currentAttackSpeed - (baseAttackSpeed / 100 * attackSpeedModificator);
            print(currentAttackSpeed);
        }

        private void DoubleAttack() {
            Debug.Log("Атакую Двойной атакой!");
        }


        //TODO Сделать атаку корутиной и поставить таймер на скорость атаки

        private void NormalAttack() {
            if(Time.time > attackSpeedTimer) {
                attackSpeedTimer = Time.time + currentAttackSpeed;
                CreateBullet();
            }
        }

        // Создание стрелы из префаба в пул
        private GameObject CreateBullet() {
            GameObject obj = ObjectsPool.Instance.GetObject(bulletsPrefab);

            obj.transform.position = player.GetFirePointTransform().position;
            obj.transform.rotation = player.transform.rotation;
            obj.transform.SetParent(playerGraphicObject.transform);
            return obj;
        }
    }
}
