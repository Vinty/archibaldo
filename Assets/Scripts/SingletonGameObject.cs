﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonGameObject<T> : MonoBehaviour where T : Component {
    private static T instance;

    public static T Instance {
        get {
            if(instance) {
                return instance;
            }
            var obj = new GameObject($"Singleton {typeof(T)}");
            instance = obj.AddComponent<T>();
            return instance;
        }
    }
}
