﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.Enemy {
    public enum StateEnemy {
        FIND_POINT,
        MOVE_TO,
        WAIT
    }

    public class EnemyMovement : MonoBehaviour {
        [SerializeField] private float       speed        = 10f;
        [SerializeField] private float       agroDistance = 10f;
        [SerializeField] private GameObject  player;
        [SerializeField] private Transform[] patrolPoints;

        private Transform    currentPlayerTransform;
        private NavMeshAgent agent;

        public StateEnemy CurrentState {get; set;}

        // private int          lenghtMovementPointIndex;
        private int lastPatrolPoint = 0;

        public bool IsStopMoving {get; set;}

        private void Start() {
            agent                  = GetComponent<NavMeshAgent>();
            CurrentState           = StateEnemy.FIND_POINT;
            agent.speed            = speed;
            currentPlayerTransform = player.transform;
        }

        private void Update() {
            switch(CurrentState) {
                case StateEnemy.FIND_POINT :
                    FindCurrentPoint();
                    CurrentState = StateEnemy.MOVE_TO;
                    break;
                case StateEnemy.MOVE_TO :
                    var distanceToPlayer = Vector3.Distance(transform.position, currentPlayerTransform.position);
                    if(distanceToPlayer < agroDistance) {
                        MoveToNearbyPlayer();
                    }
                    else {
                        Patrol();
                    }

                    break;
                case StateEnemy.WAIT : break;
            }
        }

        private void MoveToNearbyPlayer() {
            StartCoroutine(MoveToPlayer());
            CurrentState = StateEnemy.WAIT;
        }

        private void Patrol() {
            int currentPoint = Random.Range(0, patrolPoints.Length);
            // что бы не совпала точка с предыдущей
            if(lastPatrolPoint != currentPoint) {
                StartCoroutine(MoveToPoint());
                CurrentState = StateEnemy.WAIT;
            }

            lastPatrolPoint = currentPoint;
        }

        private void FindCurrentPoint() {
            currentPlayerTransform = player.transform;
        }

        private IEnumerator MoveToPlayer() {
            var playerPos = currentPlayerTransform.position;
            agent.SetDestination(playerPos);
            yield return null;
            yield return new WaitForSeconds(2f);
            CurrentState = StateEnemy.FIND_POINT;
        }

        private IEnumerator MoveToPoint() {
            agent.SetDestination(patrolPoints[lastPatrolPoint].position);
            yield return null;
            yield return new WaitForSeconds(2f);
            CurrentState = StateEnemy.FIND_POINT;
        }
    }
}
